import * as TSLOG from "tslog";
import { Configuration } from "../configuration";

const generateLoggerName = () => {
  return "Logger";
};
//Initialize logger, set min level to trace untill is changed by configuration
export const Logger = (options?: { minLevel?: TSLOG.TLogLevelName }) =>
  new TSLOG.Logger({
    name: generateLoggerName(),
    minLevel: options?.minLevel || Configuration?.proxy?.logLevel || "info",
  });
