import { load } from "js-yaml";
import { readFileSync } from "fs";
import { validateConfiguration } from "./validateConfiguration";
import * as path from "path";
import { ESignatureMiddlewareConfiguration } from "./config";
import { Logger } from "../log";

//Global object to store configuration
export let Configuration: ESignatureMiddlewareConfiguration | undefined;

/**
 * Reads configuration.yaml file and generates config global object.
 * config file must be located in root directory
 */
export function readConfigurationFileToYAML() {
  const configFilePath = path.join(__dirname, "../config.yaml");
  return load(readFileSync(configFilePath, "utf-8"));
}

export function readConfig() {
  const readConfiguration = validateConfiguration(
    readConfigurationFileToYAML()
  );
  if (readConfiguration) {
    const logger = Logger();
    logger.info("Configuration read successfully");
    Configuration = readConfiguration;
  }
}
