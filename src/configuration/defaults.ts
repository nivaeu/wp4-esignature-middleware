export const DEFAULT_CONFIGURATION = {
  PORT: 5000,
  HOST: "localhost",
  ESIGNATURE_API_BASE_URL: "/services/rest/",
};
