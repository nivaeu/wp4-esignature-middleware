import { TLogLevelName } from "tslog";

const config = {
  proxy: {
    PORT: 5000,
    HOST: "localhost",

    proxyRoutes: [
      {
        path: "security",
        target: "http://localhost:8888",
      },
    ],
  },
  "e-signature": {
    eSignatureService: {
      host: "http://localhost:8080",
      baseUrl: "/services/rest/",
    },
  },
};

export interface ESignatureMiddlewareConfiguration {
  proxy: {
    port: number;
    host: string;
    logLevel: TLogLevelName;
    proxyRoutes: ProxyRoute[];
  };
  "e-signature": {
    eSignatureService: {
      host: string;
      baseUrl: string;
    };
  };
}
export interface ProxyRoute {
  path: string;
  target: string;
}

export default config;
