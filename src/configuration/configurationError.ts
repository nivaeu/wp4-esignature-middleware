export class ConfigurationError extends Error {
  constructor(msg: string) {
    super("Configuration error: " + msg);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, ConfigurationError.prototype);
  }
}
