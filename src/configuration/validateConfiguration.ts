import { Logger } from "../log";
import { ESignatureMiddlewareConfiguration } from "./config";
import { ConfigurationError } from "./configurationError";
import { DEFAULT_CONFIGURATION } from "./defaults";

/**
 * Processes raw config read from config.yaml file and validates it creating a configuration object
 * @param rawConfig {Object} Configuration raw object
 * @return {}
 */
export function validateConfiguration(
  rawConfig: any
): ESignatureMiddlewareConfiguration | void {
  const inValidationConfig: Partial<ESignatureMiddlewareConfiguration> =
    rawConfig;
  const logger = Logger();
  logger.info("Starting configuration file validation");
  //validation of proxy configuration
  if (inValidationConfig.proxy) {
    logger.info("Validating proxy section configuration");

    const { proxy } = inValidationConfig;
    if (proxy.host !== undefined && typeof proxy.host !== "string") {
      throw new ConfigurationError(
        "Error on 'proxy' configuration section. host parameter has invalid value"
      );
    }
    if (proxy.port !== undefined && typeof proxy.port !== "number") {
      throw new ConfigurationError(
        `Error on 'proxy' configuration section. port parameter has invalid value type. Expected type 'number', type is ${typeof proxy.port}`
      );
    }
    if (proxy.port === undefined) {
      logger.info(
        `Proxy configuration section PORT parameter was found empty, using default port ${DEFAULT_CONFIGURATION.PORT}`
      );
      inValidationConfig.proxy.port = DEFAULT_CONFIGURATION.PORT;
    }
    if (proxy.host === undefined) {
      logger.info(
        `Proxy configuration section HOST parameter was found empty, using default port ${DEFAULT_CONFIGURATION.HOST}`
      );
      inValidationConfig.proxy.host = DEFAULT_CONFIGURATION.HOST;
    }
    if (proxy.proxyRoutes === undefined || !proxy.proxyRoutes.length) {
      throw new ConfigurationError(
        `Error on 'proxy' configuration section. Proxy routes array was found empty or undefined. An empty proxy routes will cause the service not to start.`
      );
    }
    //proxy routes validation
    const usedPaths: string[] = [];
    for (const proxyRouteIndex in proxy.proxyRoutes) {
      const proxyRoute = proxy.proxyRoutes[proxyRouteIndex];
      const { path, target } = proxyRoute;
      if (
        !path ||
        !target ||
        typeof path !== "string" ||
        typeof target !== "string"
      ) {
        throw new ConfigurationError(
          `Error on 'proxy' configuration section. Proxy routes array element with index ${proxyRouteIndex} has missconfigured path or target`
        );
      }
      //check taht current proxy route hasn't already been defined
      if (usedPaths.includes(path)) {
        throw new ConfigurationError(
          `Error on 'proxy' configuration section. Proxy routes array element with index ${proxyRouteIndex}. Configured path '${path}' has already been defined in element with index ${proxy.proxyRoutes.findIndex(
            (pr) => pr.path === path
          )}`
        );
      } else {
        usedPaths.push(path);
      }
    }
  }
  //validation of e-signature configuration
  if (inValidationConfig["e-signature"]) {
    logger.info("Validating 'e-signature' section configuration");
    const eSignature = inValidationConfig["e-signature"];
    if (eSignature.eSignatureService) {
      if (
        eSignature.eSignatureService.host === undefined ||
        typeof eSignature.eSignatureService.host !== "string"
      ) {
        throw new ConfigurationError(
          "Error on 'e-signature' configuration section. host parameter of subconfiguration 'eSignatureService' has invalid value"
        );
      }
      if (
        eSignature.eSignatureService.baseUrl === undefined ||
        typeof eSignature.eSignatureService.baseUrl !== "string"
      ) {
        throw new ConfigurationError(
          "Error on 'e-signature' configuration section. baseUrl parameter of subconfiguration 'eSignatureService' has invalid value"
        );
      }
    } else {
      throw new ConfigurationError(
        `Error on 'e-signature' configuration section. eSignature Service subconfiguration not found`
      );
    }
  } else {
    throw new ConfigurationError(
      `Error on 'proxy' configuration section. E-signature API configuration couldn't be found`
    );
  }
  return inValidationConfig as ESignatureMiddlewareConfiguration;
}
