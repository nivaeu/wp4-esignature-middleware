import { XmlConclusion } from "../../e-signature-dss/model/validation/typings";

export class GenericErrorResponse {


    constructor(status: "KO" | "OK", message: string, validationResult?: XmlConclusion){
        this.status = status,
        this.message = message,
        this.validationResult = validationResult
    }

    status: "OK" | "KO";
    message: string;
    validationResult?: XmlConclusion
}