import { Request, Response, NextFunction } from "express";
import { Logger } from "../log";
import { GenericErrorResponse } from "./errorResponses/genericResponse";
import { validateSignedDocumentAndRedirectRequest } from "./validateSignedDocumentAndRedirect";
import { AxiosError } from "axios";
import { validateSignedDocumentResponse } from "./validateSignedDocumentResponse";
/**
 * Function to be applied as a middleware to validate requets signed documents
 * @param req
 * @param res
 * @param next
 */
export async function signatureValidationInterceptor(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const logger = Logger();

  //check for present header of req   uest to determine wich validation method to use
  const { headers, body } = req;

  //check if request has body
  //if not, validation is only needed if request includes a body
  if (Object.keys(body).length) {
    if (headers["esignature-middleware-validate-response"] === "true") {
      logger.debug("Intercepted request with validation of response requested");
      return;
    }

    logger.debug(
      "Intercepted request with no validation of response requested"
    );
    try {
      return await validateSignedDocumentAndRedirectRequest(
        { req, res, next },
        body
      );
    } catch (err) {
      logger.error("Error on document signature validation");
      //logger.error((err as AxiosError).message);
      res
        .status(400)
        .send(
          new GenericErrorResponse(
            "KO",
            "Error on document's signature validation"
          )
        );
    }
  } else {
    // //if 'esignature-middleware-validation' is present, validation of the response of the original request must be execute
    if (headers["esignature-middleware-validate-response"] === "true") {
      return await validateSignedDocumentResponse(
        { req, res, next }
      )
    } else {
      //if not, response is just forwarded
      logger.debug(
        "Request has no body and no response validation header, request is being forwarded"
      );
      next();
      return;
    }
  }
}
