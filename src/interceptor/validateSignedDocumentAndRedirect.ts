import { NextFunction, Request, Response } from "express";
import config from "../configuration/config";
import { ValidationResultIndications } from "../e-signature-dss/model/validation/enums/validationResultIndications";
import {
  DataToValidateDTO,
  XmlConclusion,
} from "../e-signature-dss/model/validation/typings";
import { ESignatureDSSClient } from "../e-signature-dss/services";
import { validateSignedDocument } from "../e-signature-dss/services/operations/validateSignedDocument";
import { Logger } from "../log";
import { GenericErrorResponse } from "./errorResponses/genericResponse";

export async function validateSignedDocumentAndRedirectRequest(
  { req, res, next }: { req: Request; res: Response; next: NextFunction },
  documentToValidate: DataToValidateDTO
): Promise<any> {
  const logger = Logger();
  logger.debug("Validating document content");
  //valida the document
  try {
    const {baseUrl, host} = config["e-signature"].eSignatureService

    let validationResult = await validateSignedDocument(documentToValidate, { baseURl: host + baseUrl })
    console.log(validationResult)
    //e-signature-dss response includes "message" string in its error message
    if (Object.keys(validationResult).includes("message")) {
      return validationResult as Error;
    } else {
      validationResult = validationResult as XmlConclusion;

      if (
        validationResult.Indication === ValidationResultIndications.PASSED ||
        validationResult.Indication === ValidationResultIndications.TOTAL_PASSED
      ) {
        logger.debug(
          "Request validation has passed, checking response esignature validity before redirecting request"
        );

        next();
        return;
      } else {
        logger.error("Validation has failed");
        res
          .status(401)
          .send(new GenericErrorResponse("KO", "Unsuccessfull validation", validationResult));
        return;
      }
    }
  } catch (err) {
    logger.error("Error on document validation");
    //logger.error(err);
    throw err;
  }
}
