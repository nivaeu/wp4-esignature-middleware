import axios from "axios";
import { NextFunction, Request, Response } from "express";
import config from "../configuration/config";
import { ValidationResultIndications } from "../e-signature-dss/model/validation/enums/validationResultIndications";
import {
  DataToValidateDTO,
  XmlConclusion,
} from "../e-signature-dss/model/validation/typings";
import { ESignatureDSSClient } from "../e-signature-dss/services";
import { validateSignedDocument } from "../e-signature-dss/services/operations/validateSignedDocument";
import { Logger } from "../log";
import { GenericErrorResponse } from "./errorResponses/genericResponse";

/**
 * Function to validate
 * @param param0
 * @param documentToValidate
 * @returns
 */
export async function validateSignedDocumentResponse({
  req,
  res,
  next,
}: {
  req: Request;
  res: Response;
  next: NextFunction;
}): Promise<any> {
  const logger = Logger();

  logger.debug("Making original request and validating response");

  const url = `http://${config.proxy.HOST}:${config.proxy.PORT}${req.originalUrl}`;
  console.log(url);

  //make the request to original request host
  const originalResponse = await axios.get(url);
  console.log(originalResponse.data);
  try {
    const { baseUrl, host } = config["e-signature"].eSignatureService;

    let validationResult = await validateSignedDocument(originalResponse.data, {
      baseURl: host + baseUrl,
    });

    //e-signature-dss response includes "message" string in its error message
    if (Object.keys(validationResult).includes("message")) {
      return validationResult as Error;
    } else {
      validationResult = validationResult as XmlConclusion;

      if (
        validationResult.Indication === ValidationResultIndications.PASSED ||
        validationResult.Indication === ValidationResultIndications.TOTAL_PASSED
      ) {
        logger.debug(
          "Request validation has passed, checking response esignature validity before redirecting request"
        );
        res.status(201).json(originalResponse.data);
        next();
        return;
      } else {
        logger.error("Validation has failed");
        res
          .status(401)
          .send(
            new GenericErrorResponse(
              "KO",
              "Unsuccessfull validation",
              validationResult
            )
          );
        return;
      }
    }
  } catch (err) {
    logger.error("Error on document validation");
    //logger.error(err);
    throw err;
  }
}
