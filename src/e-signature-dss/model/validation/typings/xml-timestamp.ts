/* tslint:disable */
import { XmlBasicSignature } from './xml-basic-signature';
import { XmlChainItem } from './xml-chain-item';
import { XmlDigestAlgoAndValue } from './xml-digest-algo-and-value';
import { XmlDigestMatcher } from './xml-digest-matcher';
import { XmlFoundCertificates } from './xml-found-certificates';
import { XmlFoundRevocations } from './xml-found-revocations';
import { XmlPDFRevision } from './xml-pdf-revision';
import { XmlSignerInfo } from './xml-signer-info';
import { XmlSigningCertificate } from './xml-signing-certificate';
import { XmlTimestampedObject } from './xml-timestamped-object';

export interface XmlTimestamp {
  id?: string;
  timestampFilename?: string;
  archiveTimestampType?:
    | 'XAdES'
    | 'XAdES_141'
    | 'CAdES'
    | 'CAdES_V2'
    | 'CAdES_V3'
    | 'CAdES_DETACHED'
    | 'JAdES'
    | 'PAdES';
  productionTime: string;
  digestMatchers: Array<XmlDigestMatcher>;
  basicSignature: XmlBasicSignature;
  signingCertificate?: XmlSigningCertificate;
  certificateChain?: Array<XmlChainItem>;
  signerInformationStore?: Array<XmlSignerInfo>;
  foundCertificates: XmlFoundCertificates;
  foundRevocations: XmlFoundRevocations;
  timestampedObjects?: Array<XmlTimestampedObject>;
  base64Encoded?: Array<string>;
  digestAlgoAndValue?: XmlDigestAlgoAndValue;
  type?:
    | 'CONTENT_TIMESTAMP'
    | 'ALL_DATA_OBJECTS_TIMESTAMP'
    | 'INDIVIDUAL_DATA_OBJECTS_TIMESTAMP'
    | 'SIGNATURE_TIMESTAMP'
    | 'VALIDATION_DATA_REFSONLY_TIMESTAMP'
    | 'VALIDATION_DATA_TIMESTAMP'
    | 'DOCUMENT_TIMESTAMP'
    | 'ARCHIVE_TIMESTAMP';
  pdfrevision?: XmlPDFRevision;
}
