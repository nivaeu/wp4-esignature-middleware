/* tslint:disable */
export interface XmlManifestFile {
  filename?: string;
  signatureFilename?: string;
  entries?: Array<string>;
}
