/* tslint:disable */
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';
import { XmlCryptographicInformation } from './xml-cryptographic-information';

export interface XmlSAV {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
  cryptographicInfo: XmlCryptographicInformation;
  validationTime?: string;
}
