/* tslint:disable */
import { IndividualValidationConstraintReportType } from './individual-validation-constraint-report-type';
import { SignatureValidationPolicyType } from './signature-validation-policy-type';

export interface ValidationConstraintsEvaluationReportType {
  signatureValidationPolicy?: SignatureValidationPolicyType;
  validationConstraint?: Array<IndividualValidationConstraintReportType>;
}
