/* tslint:disable */
import { DigestAlgAndValueType } from './digest-alg-and-value-type';
import { ObjectIdentifierType } from './object-identifier-type';
import { SigPolicyQualifiersListType } from './sig-policy-qualifiers-list-type';
import { TransformsType } from './transforms-type';

export interface SignaturePolicyIdType {
  sigPolicyId: ObjectIdentifierType;
  transforms?: TransformsType;
  sigPolicyHash: DigestAlgAndValueType;
  sigPolicyQualifiers?: SigPolicyQualifiersListType;
}
