/* tslint:disable */
import { XmlCertificate } from './xml-certificate';
import { XmlContainerInfo } from './xml-container-info';
import { XmlOrphanTokens } from './xml-orphan-tokens';
import { XmlRevocation } from './xml-revocation';
import { XmlSignature } from './xml-signature';
import { XmlSignerData } from './xml-signer-data';
import { XmlTimestamp } from './xml-timestamp';
import { XmlTrustedList } from './xml-trusted-list';

export interface XmlDiagnosticData {
  documentName?: string;
  validationDate: string;
  containerInfo?: XmlContainerInfo;
  signatures?: Array<XmlSignature>;
  usedCertificates?: Array<XmlCertificate>;
  usedRevocations?: Array<XmlRevocation>;
  usedTimestamps?: Array<XmlTimestamp>;
  orphanTokens?: XmlOrphanTokens;
  originalDocuments?: Array<XmlSignerData>;
  trustedLists?: Array<XmlTrustedList>;
}
