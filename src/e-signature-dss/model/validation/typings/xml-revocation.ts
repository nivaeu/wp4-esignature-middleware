/* tslint:disable */
import { XmlBasicSignature } from './xml-basic-signature';
import { XmlChainItem } from './xml-chain-item';
import { XmlDigestAlgoAndValue } from './xml-digest-algo-and-value';
import { XmlFoundCertificates } from './xml-found-certificates';
import { XmlSigningCertificate } from './xml-signing-certificate';

export interface XmlRevocation {
  id?: string;
  origin:
    | 'CMS_SIGNED_DATA'
    | 'REVOCATION_VALUES'
    | 'ATTRIBUTE_REVOCATION_VALUES'
    | 'TIMESTAMP_VALIDATION_DATA'
    | 'DSS_DICTIONARY'
    | 'VRI_DICTIONARY'
    | 'ADBE_REVOCATION_INFO_ARCHIVAL'
    | 'INPUT_DOCUMENT'
    | 'EXTERNAL'
    | 'CACHED';
  type: 'CRL' | 'OCSP';
  sourceAddress?: string;
  productionDate?: string;
  thisUpdate?: string;
  nextUpdate?: string;
  expiredCertsOnCRL?: string;
  archiveCutOff?: string;
  certHashExtensionPresent?: boolean;
  certHashExtensionMatch?: boolean;
  basicSignature?: XmlBasicSignature;
  signingCertificate?: XmlSigningCertificate;
  certificateChain?: Array<XmlChainItem>;
  foundCertificates?: XmlFoundCertificates;
  base64Encoded?: Array<string>;
  digestAlgoAndValue?: XmlDigestAlgoAndValue;
}
