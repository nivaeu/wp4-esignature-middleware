/* tslint:disable */
import { XmlCertificate } from './xml-certificate';
import { XmlCertificateRef } from './xml-certificate-ref';

export interface XmlRelatedCertificate {
  origins?: Array<
    | 'KEY_INFO'
    | 'SIGNED_DATA'
    | 'CERTIFICATE_VALUES'
    | 'ATTR_AUTHORITIES_CERT_VALUES'
    | 'TIMESTAMP_VALIDATION_DATA'
    | 'DSS_DICTIONARY'
    | 'VRI_DICTIONARY'
    | 'BASIC_OCSP_RESP'
  >;
  certificateRefs?: Array<XmlCertificateRef>;
  certificate?: XmlCertificate;
}
