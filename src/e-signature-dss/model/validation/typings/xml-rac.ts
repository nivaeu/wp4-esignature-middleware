/* tslint:disable */
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';

export interface XmlRAC {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
  revocationProductionDate: string;
  id?: string;
}
