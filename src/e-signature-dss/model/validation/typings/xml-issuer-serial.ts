/* tslint:disable */
export interface XmlIssuerSerial {
  value?: Array<string>;
  match?: boolean;
}
