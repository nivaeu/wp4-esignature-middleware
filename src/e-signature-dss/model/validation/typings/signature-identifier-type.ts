/* tslint:disable */
import { DigestAlgAndValueType } from './digest-alg-and-value-type';
import { SignatureValueType } from './signature-value-type';

export interface SignatureIdentifierType {
  digestAlgAndValue?: DigestAlgAndValueType;
  signatureValue?: SignatureValueType;
  hashOnly?: boolean;
  docHashOnly?: boolean;
  id?: string;
  daidentifier?: string;
}
