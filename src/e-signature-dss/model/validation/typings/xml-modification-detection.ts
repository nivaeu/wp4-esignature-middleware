/* tslint:disable */
import { XmlModification } from './xml-modification';

export interface XmlModificationDetection {
  annotationOverlap?: Array<XmlModification>;
  visualDifference?: Array<XmlModification>;
  pageDifference?: Array<XmlModification>;
}
