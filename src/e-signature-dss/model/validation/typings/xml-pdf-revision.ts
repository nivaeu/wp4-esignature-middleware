/* tslint:disable */
import { XmlModificationDetection } from './xml-modification-detection';
import { XmlPDFSignatureDictionary } from './xml-pdf-signature-dictionary';

export interface XmlPDFRevision {
  signatureFieldName?: Array<string>;
  modificationDetection?: XmlModificationDetection;
  pdfsignatureDictionary?: XmlPDFSignatureDictionary;
}
