/* tslint:disable */
import { XmlAbstractToken } from './xml-abstract-token';

export interface XmlTimestampedObject {
  token?: XmlAbstractToken;
  category?:
    | 'SIGNED_DATA'
    | 'SIGNATURE'
    | 'CERTIFICATE'
    | 'REVOCATION'
    | 'TIMESTAMP'
    | 'ORPHAN_CERTIFICATE'
    | 'ORPHAN_REVOCATION';
}
