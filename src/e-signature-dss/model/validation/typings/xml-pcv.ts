/* tslint:disable */
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';

export interface XmlPCV {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
  controlTime?: string;
}
