/* tslint:disable */
import { DataToValidateDTO } from './data-to-validate-dto';

export type ValidateSignatureRequest<
  TCode extends 'application/json' = 'application/json'
> = TCode extends 'application/json' ? DataToValidateDTO : any;
