/* tslint:disable */
export interface XmlTrustedList {
  countryCode?: string;
  url: string;
  sequenceNumber?: number;
  version?: number;
  lastLoading?: string;
  issueDate?: string;
  nextUpdate?: string;
  wellSigned?: boolean;
  id?: string;
  lotl?: boolean;
}
