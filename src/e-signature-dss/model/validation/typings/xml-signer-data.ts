/* tslint:disable */
import { XmlDigestAlgoAndValue } from './xml-digest-algo-and-value';

export interface XmlSignerData {
  id?: string;
  referencedName?: string;
  digestAlgoAndValue: XmlDigestAlgoAndValue;
}
