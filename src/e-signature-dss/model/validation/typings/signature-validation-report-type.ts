/* tslint:disable */
import { SignatureAttributesType } from './signature-attributes-type';
import { SignatureIdentifierType } from './signature-identifier-type';
import { SignatureQualityType } from './signature-quality-type';
import { SignatureValidationProcessType } from './signature-validation-process-type';
import { SignerInformationType } from './signer-information-type';
import { SignersDocumentType } from './signers-document-type';
import { ValidationConstraintsEvaluationReportType } from './validation-constraints-evaluation-report-type';
import { ValidationStatusType } from './validation-status-type';
import { ValidationTimeInfoType } from './validation-time-info-type';

export interface SignatureValidationReportType {
  signatureIdentifier?: SignatureIdentifierType;
  validationConstraintsEvaluationReport?: ValidationConstraintsEvaluationReportType;
  validationTimeInfo?: ValidationTimeInfoType;
  signersDocument?: Array<SignersDocumentType>;
  signatureAttributes?: SignatureAttributesType;
  signerInformation?: SignerInformationType;
  signatureQuality?: SignatureQualityType;
  signatureValidationProcess?: SignatureValidationProcessType;
  signatureValidationStatus: ValidationStatusType;
  any?: Array<{
    [key: string]: any;
  }>;
}
