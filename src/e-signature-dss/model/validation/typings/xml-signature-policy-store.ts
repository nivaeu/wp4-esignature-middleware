/* tslint:disable */
import { XmlDigestAlgoAndValue } from './xml-digest-algo-and-value';

export interface XmlSignaturePolicyStore {
  id: string;
  description?: string;
  documentationReferences?: Array<string>;
  digestAlgoAndValue?: XmlDigestAlgoAndValue;
}
