/* tslint:disable */
export interface XmlStructuralValidation {
  messages?: Array<string>;
  valid?: boolean;
}
