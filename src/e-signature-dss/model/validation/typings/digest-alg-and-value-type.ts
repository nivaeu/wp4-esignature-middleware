/* tslint:disable */
import { DigestMethodType } from './digest-method-type';

export interface DigestAlgAndValueType {
  digestMethod: DigestMethodType;
  digestValue: Array<string>;
}
