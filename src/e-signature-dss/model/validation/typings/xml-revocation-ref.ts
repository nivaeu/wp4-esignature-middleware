/* tslint:disable */
import { XmlDigestAlgoAndValue } from './xml-digest-algo-and-value';
import { XmlSignerInfo } from './xml-signer-info';

export interface XmlRevocationRef {
  origins: Array<'COMPLETE_REVOCATION_REFS' | 'ATTRIBUTE_REVOCATION_REFS'>;
  digestAlgoAndValue?: XmlDigestAlgoAndValue;
  producedAt?: string;
  responderId?: XmlSignerInfo;
}
