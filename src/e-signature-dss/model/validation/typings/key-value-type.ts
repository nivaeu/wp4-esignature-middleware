/* tslint:disable */
export interface KeyValueType {
  content?: Array<{
    [key: string]: any;
  }>;
}
