/* tslint:disable */
export interface PostalAddressType {
  streetAddress: string;
  locality: string;
  stateOrProvince?: string;
  postalCode?: string;
  countryName: string;
  lang?: string;
}
