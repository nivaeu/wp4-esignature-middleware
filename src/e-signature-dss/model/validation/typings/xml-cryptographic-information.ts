/* tslint:disable */
export interface XmlCryptographicInformation {
  algorithm: string;
  keyLength?: string;
  secure?: boolean;
  notAfter?: string;
  concernedMaterial: string;
}
