/* tslint:disable */
export interface XmlSignerDocumentRepresentations {
  hashOnly?: boolean;
  docHashOnly?: boolean;
}
