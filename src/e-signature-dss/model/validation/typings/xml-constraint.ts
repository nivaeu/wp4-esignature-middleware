/* tslint:disable */
import { XmlName } from './xml-name';

export interface XmlConstraint {
  name: XmlName;
  status: 'OK' | 'NOT_OK' | 'IGNORED' | 'INFORMATION' | 'WARNING';
  error?: XmlName;
  warning?: XmlName;
  info?: XmlName;
  additionalInfo?: string;
  id?: string;
}
