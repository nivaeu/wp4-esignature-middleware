/* tslint:disable */
import { AdditionalValidationReportDataType } from './additional-validation-report-data-type';
import { CertificateChainType } from './certificate-chain-type';
import { CryptoInformationType } from './crypto-information-type';
import { RevocationStatusInformationType } from './revocation-status-information-type';
import { VOReferenceType } from './vo-reference-type';

export interface ValidationReportDataType {
  trustAnchor?: VOReferenceType;
  certificateChain?: CertificateChainType;
  relatedValidationObject?: Array<VOReferenceType>;
  revocationStatusInformation?: RevocationStatusInformationType;
  cryptoInformation?: CryptoInformationType;
  additionalValidationReportData?: AdditionalValidationReportDataType;
}
