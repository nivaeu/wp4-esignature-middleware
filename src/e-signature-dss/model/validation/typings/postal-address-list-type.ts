/* tslint:disable */
import { PostalAddressType } from './postal-address-type';

export interface PostalAddressListType {
  postalAddress: Array<PostalAddressType>;
}
