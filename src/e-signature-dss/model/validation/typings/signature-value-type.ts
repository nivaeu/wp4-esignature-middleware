/* tslint:disable */
export interface SignatureValueType {
  value?: Array<string>;
  id?: string;
}
