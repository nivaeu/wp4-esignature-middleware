/* tslint:disable */
import { XmlTimestamp } from './xml-timestamp';

export interface XmlFoundTimestamp {
  timestamp?: XmlTimestamp;
}
