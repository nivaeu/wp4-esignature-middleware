/* tslint:disable */
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';
import { XmlRAC } from './xml-rac';
import { XmlRFC } from './xml-rfc';
import { XmlRevocationInformation } from './xml-revocation-information';

export interface XmlSubXCV {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
  crossCertificates?: Array<string>;
  equivalentCertificates?: Array<string>;
  rac?: Array<XmlRAC>;
  rfc?: XmlRFC;
  revocationInfo?: XmlRevocationInformation;
  id?: string;
  trustAnchor?: boolean;
  selfSigned?: boolean;
}
