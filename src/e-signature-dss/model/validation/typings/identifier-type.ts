/* tslint:disable */
export interface IdentifierType {
  value?: string;
  qualifier?: 'OID_AS_URI' | 'OID_AS_URN';
}
