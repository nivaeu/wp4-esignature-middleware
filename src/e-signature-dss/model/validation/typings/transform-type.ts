/* tslint:disable */
export interface TransformType {
  content?: Array<{
    [key: string]: any;
  }>;
  algorithm?: string;
}
