/* tslint:disable */
import { XmlCertificateChain } from './xml-certificate-chain';

export interface XmlToken {
  filename?: string;
  certificateChain?: XmlCertificateChain;
  indication:
    | 'TOTAL_PASSED'
    | 'TOTAL_FAILED'
    | 'INDETERMINATE'
    | 'PASSED'
    | 'FAILED'
    | 'NO_SIGNATURE_FOUND';
  subIndication?:
    | 'FORMAT_FAILURE'
    | 'HASH_FAILURE'
    | 'SIG_CRYPTO_FAILURE'
    | 'REVOKED'
    | 'SIG_CONSTRAINTS_FAILURE'
    | 'CHAIN_CONSTRAINTS_FAILURE'
    | 'CERTIFICATE_CHAIN_GENERAL_FAILURE'
    | 'CRYPTO_CONSTRAINTS_FAILURE'
    | 'EXPIRED'
    | 'NOT_YET_VALID'
    | 'POLICY_PROCESSING_ERROR'
    | 'SIGNATURE_POLICY_NOT_AVAILABLE'
    | 'TIMESTAMP_ORDER_FAILURE'
    | 'NO_SIGNING_CERTIFICATE_FOUND'
    | 'NO_CERTIFICATE_CHAIN_FOUND'
    | 'REVOKED_NO_POE'
    | 'REVOKED_CA_NO_POE'
    | 'OUT_OF_BOUNDS_NO_POE'
    | 'OUT_OF_BOUNDS_NOT_REVOKED'
    | 'CRYPTO_CONSTRAINTS_FAILURE_NO_POE'
    | 'NO_POE'
    | 'TRY_LATER'
    | 'SIGNED_DATA_NOT_FOUND';
  errors?: Array<string>;
  warnings?: Array<string>;
  infos?: Array<string>;
  id?: string;
}
