/* tslint:disable */
export interface DigestMethodType {
  content?: Array<{
    [key: string]: any;
  }>;
  algorithm?: string;
}
