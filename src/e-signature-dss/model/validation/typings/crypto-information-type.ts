/* tslint:disable */
import { TypedDataType } from './typed-data-type';
import { VOReferenceType } from './vo-reference-type';

export interface CryptoInformationType {
  validationObjectId: VOReferenceType;
  algorithm: string;
  algorithmParameters?: TypedDataType;
  secureAlgorithm?: boolean;
  notAfter?: string;
}
