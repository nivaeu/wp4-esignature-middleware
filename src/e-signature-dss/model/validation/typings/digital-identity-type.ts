/* tslint:disable */
import { AnyType } from './any-type';
import { KeyValueType } from './key-value-type';

export interface DigitalIdentityType {
  x509Certificate?: Array<string>;
  x509SubjectName?: string;
  keyValue?: KeyValueType;
  x509SKI?: Array<string>;
  other?: AnyType;
}
