/* tslint:disable */
import { DocumentationReferencesType } from './documentation-references-type';
import { IdentifierType } from './identifier-type';

export interface ObjectIdentifierType {
  identifier: IdentifierType;
  description?: string;
  documentationReferences?: DocumentationReferencesType;
}
