/* tslint:disable */
import { DigitalIdentityType } from './digital-identity-type';
import { TSPInformationType } from './tsp-information-type';

export interface SignatureValidatorType {
  digitalId: Array<DigitalIdentityType>;
  tspinformation?: TSPInformationType;
}
