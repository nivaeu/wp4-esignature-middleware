/* tslint:disable */
import { XmlDigestAlgoAndValue } from './xml-digest-algo-and-value';

export interface XmlPolicy {
  id: string;
  url?: string;
  description?: string;
  notice?: string;
  zeroHash?: boolean;
  digestAlgoAndValue?: XmlDigestAlgoAndValue;
  asn1Processable?: boolean;
  transformations?: Array<string>;
  identified?: boolean;
  status?: boolean;
  processingError?: string;
  digestAlgorithmsEqual?: boolean;
  documentationReferences?: Array<string>;
}
