/* tslint:disable */
import { RemoteDocument } from './remote-document';

export interface DataToValidateDTO {
  signedDocument?: RemoteDocument;
  originalDocuments?: Array<RemoteDocument>;
  policy?: RemoteDocument;
  tokenExtractionStrategy?:
    | 'EXTRACT_ALL'
    | 'EXTRACT_CERTIFICATES_ONLY'
    | 'EXTRACT_TIMESTAMPS_ONLY'
    | 'EXTRACT_REVOCATION_DATA_ONLY'
    | 'EXTRACT_CERTIFICATES_AND_TIMESTAMPS'
    | 'EXTRACT_CERTIFICATES_AND_REVOCATION_DATA'
    | 'EXTRACT_TIMESTAMPS_AND_REVOCATION_DATA'
    | 'NONE';
  signatureId?: string;
}
