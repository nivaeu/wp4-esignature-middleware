/* tslint:disable */
import { SignaturePolicyIdentifierType } from './signature-policy-identifier-type';
import { VOReferenceType } from './vo-reference-type';

export interface SignatureValidationPolicyType {
  signaturePolicyIdentifier: SignaturePolicyIdentifierType;
  policyName?: string;
  formalPolicyURI?: string;
  readablePolicyURI?: string;
  formalPolicyObject?: VOReferenceType;
}
