/* tslint:disable */
import { VOReferenceType } from './vo-reference-type';

export interface CertificateChainType {
  signingCertificate: VOReferenceType;
  intermediateCertificate?: Array<VOReferenceType>;
  trustAnchor?: VOReferenceType;
}
