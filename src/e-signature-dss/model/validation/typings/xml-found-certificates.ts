/* tslint:disable */
import { XmlOrphanCertificate } from './xml-orphan-certificate';
import { XmlRelatedCertificate } from './xml-related-certificate';

export interface XmlFoundCertificates {
  relatedCertificates?: Array<XmlRelatedCertificate>;
  orphanCertificates?: Array<XmlOrphanCertificate>;
}
