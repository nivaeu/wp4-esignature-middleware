/* tslint:disable */
export interface XmlSignerInfo {
  issuerName?: string;
  serialNumber?: number;
  ski?: Array<string>;
  current?: boolean;
}
