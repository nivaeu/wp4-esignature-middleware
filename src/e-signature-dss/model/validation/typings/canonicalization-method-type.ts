/* tslint:disable */
export interface CanonicalizationMethodType {
  content?: Array<{
    [key: string]: any;
  }>;
  algorithm?: string;
}
