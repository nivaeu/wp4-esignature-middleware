/* tslint:disable */
import { DigestMethodType } from './digest-method-type';
import { TransformsType } from './transforms-type';

export interface ReferenceType {
  transforms?: TransformsType;
  digestMethod: DigestMethodType;
  digestValue: Array<string>;
  id?: string;
  uri?: string;
  type?: string;
}
