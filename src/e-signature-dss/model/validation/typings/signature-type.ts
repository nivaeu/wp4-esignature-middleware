/* tslint:disable */
import { KeyInfoType } from './key-info-type';
import { ObjectType } from './object-type';
import { SignatureValueType } from './signature-value-type';
import { SignedInfoType } from './signed-info-type';

export interface SignatureType {
  signedInfo: SignedInfoType;
  signatureValue: SignatureValueType;
  keyInfo?: KeyInfoType;
  object?: Array<ObjectType>;
  id?: string;
}
