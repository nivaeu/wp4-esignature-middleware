/* tslint:disable */
import { XmlOrphanRevocationToken } from './xml-orphan-revocation-token';
import { XmlRevocationRef } from './xml-revocation-ref';

export interface XmlOrphanRevocation {
  type: 'CRL' | 'OCSP';
  origins?: Array<
    | 'CMS_SIGNED_DATA'
    | 'REVOCATION_VALUES'
    | 'ATTRIBUTE_REVOCATION_VALUES'
    | 'TIMESTAMP_VALIDATION_DATA'
    | 'DSS_DICTIONARY'
    | 'VRI_DICTIONARY'
    | 'ADBE_REVOCATION_INFO_ARCHIVAL'
    | 'INPUT_DOCUMENT'
    | 'EXTERNAL'
    | 'CACHED'
  >;
  revocationRefs?: Array<XmlRevocationRef>;
  token?: XmlOrphanRevocationToken;
}
