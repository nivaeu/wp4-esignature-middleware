/* tslint:disable */
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';

export interface XmlCV {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
}
