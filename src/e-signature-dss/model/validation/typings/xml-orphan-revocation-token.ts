/* tslint:disable */
import { XmlDigestAlgoAndValue } from './xml-digest-algo-and-value';

export interface XmlOrphanRevocationToken {
  id?: string;
  base64Encoded?: Array<string>;
  digestAlgoAndValue?: XmlDigestAlgoAndValue;
  type: 'CRL' | 'OCSP';
}
