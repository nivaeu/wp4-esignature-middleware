/* tslint:disable */
export interface XmlPDFSignatureDictionary {
  signerName?: string;
  type?: string;
  filter?: string;
  subFilter?: string;
  contactInfo?: string;
  location?: string;
  reason?: string;
  signatureByteRange?: Array<number>;
}
