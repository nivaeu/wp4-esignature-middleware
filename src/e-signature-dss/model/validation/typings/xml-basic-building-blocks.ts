/* tslint:disable */
import { XmlCV } from './xml-cv';
import { XmlCertificateChain } from './xml-certificate-chain';
import { XmlConclusion } from './xml-conclusion';
import { XmlFC } from './xml-fc';
import { XmlISC } from './xml-isc';
import { XmlPCV } from './xml-pcv';
import { XmlPSV } from './xml-psv';
import { XmlSAV } from './xml-sav';
import { XmlVCI } from './xml-vci';
import { XmlVTS } from './xml-vts';
import { XmlXCV } from './xml-xcv';

export interface XmlBasicBuildingBlocks {
  fc?: XmlFC;
  isc?: XmlISC;
  vci?: XmlVCI;
  xcv?: XmlXCV;
  cv?: XmlCV;
  sav?: XmlSAV;
  psv?: XmlPSV;
  pcv?: XmlPCV;
  vts?: XmlVTS;
  certificateChain?: XmlCertificateChain;
  Conclusion: XmlConclusion;
  id?: string;
  type?:
    | 'SIGNATURE'
    | 'COUNTER_SIGNATURE'
    | 'TIMESTAMP'
    | 'REVOCATION'
    | 'CERTIFICATE';
}
