/* tslint:disable */
import { WSReportsDTO } from './ws-reports-dto';

export type ValidateSignatureResponse<
  TCode extends 'default' = 'default',
  TContentType extends 'application/json' = 'application/json'
> = TCode extends 'default'
  ? TContentType extends 'application/json'
    ? WSReportsDTO
    : any
  : any;
