/* tslint:disable */
import { TransformType } from './transform-type';

export interface TransformsType {
  transform: Array<TransformType>;
}
