/* tslint:disable */
import { TypedDataType } from './typed-data-type';

export interface AdditionalValidationReportDataType {
  reportData: Array<TypedDataType>;
}
