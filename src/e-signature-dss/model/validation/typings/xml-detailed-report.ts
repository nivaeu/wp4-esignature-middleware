/* tslint:disable */
import { XmlBasicBuildingBlocks } from './xml-basic-building-blocks';
import { XmlSemantic } from './xml-semantic';
import { XmlTLAnalysis } from './xml-tl-analysis';

export interface XmlDetailedReport {
  signatureOrTimestampOrCertificate?: Array<{
    [key: string]: any;
  }>;
  BasicBuildingBlocks?: Array<XmlBasicBuildingBlocks>;
  Semantic?: Array<XmlSemantic>;
  ValidationTime?: string;
  TLAnalysis?: Array<XmlTLAnalysis>;
}
