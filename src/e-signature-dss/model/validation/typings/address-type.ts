/* tslint:disable */
import { ElectronicAddressType } from './electronic-address-type';
import { PostalAddressListType } from './postal-address-list-type';

export interface AddressType {
  postalAddresses: PostalAddressListType;
  electronicAddress: ElectronicAddressType;
}
