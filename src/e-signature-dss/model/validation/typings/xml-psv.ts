/* tslint:disable */
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';

export interface XmlPSV {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
  controlTime?: string;
}
