/* tslint:disable */
import { XmlDigestAlgoAndValue } from './xml-digest-algo-and-value';
import { XmlIssuerSerial } from './xml-issuer-serial';
import { XmlSignerInfo } from './xml-signer-info';

export interface XmlCertificateRef {
  origin:
    | 'ATTRIBUTE_CERTIFICATE_REFS'
    | 'COMPLETE_CERTIFICATE_REFS'
    | 'SIGNING_CERTIFICATE';
  issuerSerial?: XmlIssuerSerial;
  digestAlgoAndValue?: XmlDigestAlgoAndValue;
  serialInfo?: XmlSignerInfo;
}
