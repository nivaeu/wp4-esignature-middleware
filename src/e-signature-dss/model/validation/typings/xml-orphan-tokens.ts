/* tslint:disable */
import { XmlOrphanCertificateToken } from './xml-orphan-certificate-token';
import { XmlOrphanRevocationToken } from './xml-orphan-revocation-token';

export interface XmlOrphanTokens {
  orphanCertificates?: Array<XmlOrphanCertificateToken>;
  orphanRevocations?: Array<XmlOrphanRevocationToken>;
}
