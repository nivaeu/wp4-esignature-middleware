/* tslint:disable */
import { SignatureReferenceType } from './signature-reference-type';
import { VOReferenceType } from './vo-reference-type';

export interface POEProvisioningType {
  validationObject?: Array<VOReferenceType>;
  signatureReference?: Array<SignatureReferenceType>;
  poetime?: string;
}
