/* tslint:disable */
import { DigestAlgAndValueType } from './digest-alg-and-value-type';
import { VOReferenceType } from './vo-reference-type';

export interface SignersDocumentType {
  digestAlgAndValue: DigestAlgAndValueType;
  signersDocumentRef?: VOReferenceType;
}
