/* tslint:disable */
import { XmlCertificateChain } from './xml-certificate-chain';
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';

export interface XmlISC {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
  certificateChain: XmlCertificateChain;
}
