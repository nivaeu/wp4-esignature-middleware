/* tslint:disable */
export interface SignatureAttributesType {
  signingTimeOrSigningCertificateOrDataObjectFormat?: Array<{
    [key: string]: any;
  }>;
}
