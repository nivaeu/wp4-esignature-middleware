/* tslint:disable */
import { POEType } from './poe-type';

export interface ValidationTimeInfoType {
  validationTime: string;
  bestSignatureTime: POEType;
}
