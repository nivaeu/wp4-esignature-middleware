/* tslint:disable */
import { XAdESSignaturePtrType } from './x-ad-es-signature-ptr-type';

export interface SignatureReferenceType {
  canonicalizationMethod?: string;
  digestMethod?: string;
  digestValue?: Array<string>;
  padESFieldName?: string;
  xadESSignaturePtr?: XAdESSignaturePtrType;
}
