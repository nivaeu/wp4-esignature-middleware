/* tslint:disable */
import { MultiLangNormStringType } from './multi-lang-norm-string-type';

export interface InternationalNamesType {
  name: Array<MultiLangNormStringType>;
}
