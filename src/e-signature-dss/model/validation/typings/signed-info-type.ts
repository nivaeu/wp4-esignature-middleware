/* tslint:disable */
import { CanonicalizationMethodType } from './canonicalization-method-type';
import { ReferenceType } from './reference-type';
import { SignatureMethodType } from './signature-method-type';

export interface SignedInfoType {
  canonicalizationMethod: CanonicalizationMethodType;
  signatureMethod: SignatureMethodType;
  reference: Array<ReferenceType>;
  id?: string;
}
