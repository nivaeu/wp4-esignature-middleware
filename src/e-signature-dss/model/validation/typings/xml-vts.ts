/* tslint:disable */
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';

export interface XmlVTS {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
  controlTime?: string;
}
