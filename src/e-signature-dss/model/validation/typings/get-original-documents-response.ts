/* tslint:disable */
import { RemoteDocument } from './remote-document';

export type GetOriginalDocumentsResponse<
  TCode extends 'default' = 'default',
  TContentType extends 'application/json' = 'application/json'
> = TCode extends 'default'
  ? TContentType extends 'application/json'
    /**
     * default response
     */
    ? Array<RemoteDocument>
    : any
  : any;
