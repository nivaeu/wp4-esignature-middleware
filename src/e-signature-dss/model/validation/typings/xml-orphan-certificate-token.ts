/* tslint:disable */
import { XmlDigestAlgoAndValue } from './xml-digest-algo-and-value';

export interface XmlOrphanCertificateToken {
  id?: string;
  base64Encoded?: Array<string>;
  digestAlgoAndValue?: XmlDigestAlgoAndValue;
}
