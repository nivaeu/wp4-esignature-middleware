/* tslint:disable */
import { VOReferenceType } from './vo-reference-type';

export interface SignerInformationType {
  signerCertificate: VOReferenceType;
  signer?: string;
  any?: {
    [key: string]: any;
  };
  pseudonym?: boolean;
}
