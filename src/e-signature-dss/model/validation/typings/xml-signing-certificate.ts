/* tslint:disable */
import { XmlCertificate } from './xml-certificate';

export interface XmlSigningCertificate {
  publicKey?: Array<string>;
  certificate?: XmlCertificate;
}
