/* tslint:disable */
import { VOReferenceType } from './vo-reference-type';

export interface RevocationStatusInformationType {
  validationObjectId: VOReferenceType;
  revocationTime: string;
  revocationReason?:
    | 'UNSPECIFIED'
    | 'KEY_COMPROMISE'
    | 'CA_COMPROMISE'
    | 'AFFILIATION_CHANGED'
    | 'SUPERSEDED'
    | 'CESSATION_OF_OPERATION'
    | 'CERTIFICATE_HOLD'
    | 'REMOVE_FROM_CRL'
    | 'PRIVILEGE_WITHDRAWN'
    | 'AA_COMPROMISE';
  revocationObject?: VOReferenceType;
}
