/* tslint:disable */
import { SignaturePolicyIdType } from './signature-policy-id-type';

export interface SignaturePolicyIdentifierType {
  signaturePolicyId?: SignaturePolicyIdType;
  signaturePolicyImplied?: {
    [key: string]: any;
  };
}
