/* tslint:disable */
import { XmlSignerData } from './xml-signer-data';

export interface XmlSignatureScope {
  scope: 'FULL' | 'PARTIAL' | 'DIGEST' | 'ARCHIVED' | 'COUNTER_SIGNATURE';
  name?: string;
  description: string;
  transformations?: Array<string>;
  signerData?: XmlSignerData;
}
