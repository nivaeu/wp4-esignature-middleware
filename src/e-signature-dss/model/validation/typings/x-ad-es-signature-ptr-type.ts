/* tslint:disable */
import { NsPrefixMappingType } from './ns-prefix-mapping-type';

export interface XAdESSignaturePtrType {
  nsPrefixMapping?: Array<NsPrefixMappingType>;
  whichDocument?: {
    [key: string]: any;
  };
  schemaRefs?: Array<{
    [key: string]: any;
  }>;
  xpath?: string;
}
