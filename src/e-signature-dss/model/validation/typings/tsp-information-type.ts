/* tslint:disable */
import { AddressType } from './address-type';
import { ExtensionsListType } from './extensions-list-type';
import { InternationalNamesType } from './international-names-type';
import { NonEmptyMultiLangURIListType } from './non-empty-multi-lang-uri-list-type';

export interface TSPInformationType {
  tspname?: InternationalNamesType;
  tsptradeName?: InternationalNamesType;
  tspaddress?: AddressType;
  tspinformationURI?: NonEmptyMultiLangURIListType;
  tspinformationExtensions?: ExtensionsListType;
}
