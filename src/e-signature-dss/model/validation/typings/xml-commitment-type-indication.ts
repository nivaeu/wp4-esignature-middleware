/* tslint:disable */
export interface XmlCommitmentTypeIndication {
  identifier: string;
  description?: string;
  documentationReferences?: Array<string>;
}
