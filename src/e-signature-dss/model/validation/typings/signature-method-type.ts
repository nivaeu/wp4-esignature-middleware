/* tslint:disable */
export interface SignatureMethodType {
  content?: Array<{
    [key: string]: any;
  }>;
  algorithm?: string;
}
