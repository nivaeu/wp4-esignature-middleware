/* tslint:disable */
export interface ObjectType {
  content?: Array<{
    [key: string]: any;
  }>;
  id?: string;
  mimeType?: string;
  encoding?: string;
}
