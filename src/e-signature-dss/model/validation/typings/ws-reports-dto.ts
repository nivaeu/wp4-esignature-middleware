/* tslint:disable */
import { ValidationReportType } from './validation-report-type';
import { XmlDetailedReport } from './xml-detailed-report';
import { XmlDiagnosticData } from './xml-diagnostic-data';
import { XmlSimpleReport } from './xml-simple-report';

export interface WSReportsDTO {
  DiagnosticData?: XmlDiagnosticData;
  SimpleReport?: XmlSimpleReport;
  DetailedReport?: XmlDetailedReport;
  validationReportDataHandler?: {
    dataSource?: {
      name?: string;
      inputStream?: {
        [key: string]: any;
      };
      contentType?: string;
      outputStream?: {
        [key: string]: any;
      };
    };
    name?: string;
    inputStream?: {
      [key: string]: any;
    };
    content?: {
      [key: string]: any;
    };
    contentType?: string;
    outputStream?: {
      [key: string]: any;
    };
    commandMap?: {
      mimeTypes?: Array<string>;
    };
    transferDataFlavors?: Array<{
      mimeType?: string;
      humanPresentableName?: string;
      primaryType?: string;
      subType?: string;
      mimeTypeSerializedObject?: boolean;
      defaultRepresentationClassAsString?: string;
      representationClassInputStream?: boolean;
      representationClassReader?: boolean;
      representationClassCharBuffer?: boolean;
      representationClassByteBuffer?: boolean;
      representationClassSerializable?: boolean;
      representationClassRemote?: boolean;
      flavorSerializedObjectType?: boolean;
      flavorRemoteObjectType?: boolean;
      flavorJavaFileListType?: boolean;
      flavorTextType?: boolean;
    }>;
    preferredCommands?: Array<{
      commandName?: string;
      commandClass?: string;
    }>;
    allCommands?: Array<{
      commandName?: string;
      commandClass?: string;
    }>;
  };
}
