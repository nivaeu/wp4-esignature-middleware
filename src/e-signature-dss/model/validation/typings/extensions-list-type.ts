/* tslint:disable */
import { ExtensionType } from './extension-type';

export interface ExtensionsListType {
  extension: Array<ExtensionType>;
}
