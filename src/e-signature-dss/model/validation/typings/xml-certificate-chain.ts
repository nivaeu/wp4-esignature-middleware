/* tslint:disable */
import { XmlChainItem } from './xml-chain-item';

export interface XmlCertificateChain {
  chainItem?: Array<XmlChainItem>;
}
