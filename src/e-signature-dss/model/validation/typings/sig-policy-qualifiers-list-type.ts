/* tslint:disable */
import { AnyType } from './any-type';

export interface SigPolicyQualifiersListType {
  sigPolicyQualifier: Array<AnyType>;
}
