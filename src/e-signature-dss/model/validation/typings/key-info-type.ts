/* tslint:disable */
export interface KeyInfoType {
  content?: Array<{
    [key: string]: any;
  }>;
  id?: string;
}
