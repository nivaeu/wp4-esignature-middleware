/* tslint:disable */
import { SignatureType } from './signature-type';
import { SignatureValidationReportType } from './signature-validation-report-type';
import { SignatureValidatorType } from './signature-validator-type';
import { ValidationObjectListType } from './validation-object-list-type';

export interface ValidationReportType {
  signatureValidationReport: Array<SignatureValidationReportType>;
  signatureValidationObjects?: ValidationObjectListType;
  signatureValidator?: SignatureValidatorType;
  signature?: SignatureType;
}
