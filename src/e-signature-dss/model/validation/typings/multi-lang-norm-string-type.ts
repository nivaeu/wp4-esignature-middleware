/* tslint:disable */
export interface MultiLangNormStringType {
  value?: string;
  lang?: string;
}
