/* tslint:disable */
import { VOReferenceType } from './vo-reference-type';

export interface POEType {
  typeOfProof: 'VALIDATION' | 'PROVIDED' | 'POLICY';
  poetime?: string;
  poeobject?: VOReferenceType;
}
