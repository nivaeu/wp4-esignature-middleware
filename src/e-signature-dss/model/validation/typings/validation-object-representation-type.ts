/* tslint:disable */
import { DigestAlgAndValueType } from './digest-alg-and-value-type';

export interface ValidationObjectRepresentationType {
  direct?: {
    [key: string]: any;
  };
  base64?: Array<string>;
  digestAlgAndValue?: DigestAlgAndValueType;
  uri?: string;
}
