/* tslint:disable */
export interface ExtensionType {
  content?: Array<{
    [key: string]: any;
  }>;
  critical?: boolean;
}
