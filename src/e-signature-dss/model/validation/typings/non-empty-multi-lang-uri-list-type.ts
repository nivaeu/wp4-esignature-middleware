/* tslint:disable */
import { NonEmptyMultiLangURIType } from './non-empty-multi-lang-uri-type';

export interface NonEmptyMultiLangURIListType {
  uri: Array<NonEmptyMultiLangURIType>;
}
