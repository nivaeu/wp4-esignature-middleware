/* tslint:disable */
export interface SignatureValidationProcessType {
  signatureValidationProcessID?: 'BASIC' | 'LTVM' | 'LTA';
  signatureValidationServicePolicy?: string;
  signatureValidationPracticeStatement?: string;
  any?: {
    [key: string]: any;
  };
}
