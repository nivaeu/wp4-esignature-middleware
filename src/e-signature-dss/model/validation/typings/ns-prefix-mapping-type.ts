/* tslint:disable */
export interface NsPrefixMappingType {
  namespaceURI: string;
  namespacePrefix: string;
}
