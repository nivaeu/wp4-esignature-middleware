/* tslint:disable */
export interface NonEmptyMultiLangURIType {
  value?: string;
  lang?: string;
}
