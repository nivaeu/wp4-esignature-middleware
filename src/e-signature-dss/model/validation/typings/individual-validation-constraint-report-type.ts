/* tslint:disable */
import { ConstraintStatusType } from './constraint-status-type';
import { TypedDataType } from './typed-data-type';
import { ValidationStatusType } from './validation-status-type';

export interface IndividualValidationConstraintReportType {
  validationConstraintIdentifier: string;
  validationConstraintParameter?: Array<TypedDataType>;
  constraintStatus: ConstraintStatusType;
  validationStatus?: ValidationStatusType;
  indications?: {
    [key: string]: any;
  };
}
