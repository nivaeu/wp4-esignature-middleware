/* tslint:disable */
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';
import { XmlSubXCV } from './xml-sub-xcv';

export interface XmlXCV {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
  subXCV?: Array<XmlSubXCV>;
}
