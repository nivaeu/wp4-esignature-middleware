/* tslint:disable */
import { XmlCertificateRef } from './xml-certificate-ref';
import { XmlOrphanCertificateToken } from './xml-orphan-certificate-token';

export interface XmlOrphanCertificate {
  origins?: Array<
    | 'KEY_INFO'
    | 'SIGNED_DATA'
    | 'CERTIFICATE_VALUES'
    | 'ATTR_AUTHORITIES_CERT_VALUES'
    | 'TIMESTAMP_VALIDATION_DATA'
    | 'DSS_DICTIONARY'
    | 'VRI_DICTIONARY'
    | 'BASIC_OCSP_RESP'
  >;
  certificateRefs?: Array<XmlCertificateRef>;
  token?: XmlOrphanCertificateToken;
}
