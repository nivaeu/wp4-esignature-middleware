/* tslint:disable */
import { NonEmptyMultiLangURIType } from './non-empty-multi-lang-uri-type';

export interface ElectronicAddressType {
  uri: Array<NonEmptyMultiLangURIType>;
}
