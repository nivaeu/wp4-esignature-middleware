/* tslint:disable */
import { XmlOrphanRevocation } from './xml-orphan-revocation';
import { XmlRelatedRevocation } from './xml-related-revocation';

export interface XmlFoundRevocations {
  relatedRevocations?: Array<XmlRelatedRevocation>;
  orphanRevocations?: Array<XmlOrphanRevocation>;
}
