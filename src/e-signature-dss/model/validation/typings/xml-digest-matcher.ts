/* tslint:disable */
export interface XmlDigestMatcher {
  digestMethod?:
    | 'SHA1'
    | 'SHA224'
    | 'SHA256'
    | 'SHA384'
    | 'SHA512'
    | 'SHA3_224'
    | 'SHA3_256'
    | 'SHA3_384'
    | 'SHA3_512'
    | 'SHAKE128'
    | 'SHAKE256'
    | 'SHAKE256_512'
    | 'RIPEMD160'
    | 'MD2'
    | 'MD5'
    | 'WHIRLPOOL';
  digestValue?: Array<string>;
  match?: boolean;
  dataFound?: boolean;
  dataIntact?: boolean;
  type?:
    | 'REFERENCE'
    | 'OBJECT'
    | 'MANIFEST'
    | 'SIGNED_PROPERTIES'
    | 'KEY_INFO'
    | 'SIGNATURE_PROPERTIES'
    | 'XPOINTER'
    | 'MANIFEST_ENTRY'
    | 'COUNTER_SIGNATURE'
    | 'MESSAGE_DIGEST'
    | 'CONTENT_DIGEST'
    | 'JWS_SIGNING_INPUT_DIGEST'
    | 'SIG_D_ENTRY'
    | 'MESSAGE_IMPRINT';
  name?: string;
  duplicated?: boolean;
}
