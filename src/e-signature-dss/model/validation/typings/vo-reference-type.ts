/* tslint:disable */
export interface VOReferenceType {
  any?: {
    [key: string]: any;
  };
  voreference?: Array<{
    [key: string]: any;
  }>;
}
