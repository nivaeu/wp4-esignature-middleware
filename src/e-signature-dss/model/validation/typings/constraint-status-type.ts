/* tslint:disable */
export interface ConstraintStatusType {
  status: 'APPLIED' | 'DISABLED' | 'OVERRIDDEN';
  overriddenBy?: string;
}
