/* tslint:disable */
export interface XmlChainItem {
  source:
    | 'TRUSTED_STORE'
    | 'TRUSTED_LIST'
    | 'SIGNATURE'
    | 'OCSP_RESPONSE'
    | 'OTHER'
    | 'AIA'
    | 'TIMESTAMP'
    | 'UNKNOWN';
  id?: string;
}
