/* tslint:disable */
export interface TypedDataType {
  type: string;
  value: {
    [key: string]: any;
  };
}
