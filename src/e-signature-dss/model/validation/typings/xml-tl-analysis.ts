/* tslint:disable */
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';

export interface XmlTLAnalysis {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
  countryCode?: string;
  url?: string;
  id?: string;
}
