/* tslint:disable */
import { XmlBasicSignature } from './xml-basic-signature';
import { XmlChainItem } from './xml-chain-item';
import { XmlCommitmentTypeIndication } from './xml-commitment-type-indication';
import { XmlDigestAlgoAndValue } from './xml-digest-algo-and-value';
import { XmlDigestMatcher } from './xml-digest-matcher';
import { XmlFoundCertificates } from './xml-found-certificates';
import { XmlFoundRevocations } from './xml-found-revocations';
import { XmlFoundTimestamp } from './xml-found-timestamp';
import { XmlPDFRevision } from './xml-pdf-revision';
import { XmlPolicy } from './xml-policy';
import { XmlSignatureDigestReference } from './xml-signature-digest-reference';
import { XmlSignaturePolicyStore } from './xml-signature-policy-store';
import { XmlSignatureProductionPlace } from './xml-signature-production-place';
import { XmlSignatureScope } from './xml-signature-scope';
import { XmlSignerDocumentRepresentations } from './xml-signer-document-representations';
import { XmlSignerInfo } from './xml-signer-info';
import { XmlSignerRole } from './xml-signer-role';
import { XmlSigningCertificate } from './xml-signing-certificate';
import { XmlStructuralValidation } from './xml-structural-validation';

export interface XmlSignature {
  id?: string;
  signatureFilename: string;
  errorMessage?: string;
  claimedSigningTime?: string;
  signatureFormat:
    | 'XML-NOT-ETSI'
    | 'XAdES-C'
    | 'XAdES-X'
    | 'XAdES-XL'
    | 'XAdES-A'
    | 'XAdES-BASELINE-LTA'
    | 'XAdES-BASELINE-LT'
    | 'XAdES-BASELINE-T'
    | 'XAdES-BASELINE-B'
    | 'CMS-NOT-ETSI'
    | 'CAdES-BASELINE-LTA'
    | 'CAdES-BASELINE-LT'
    | 'CAdES-BASELINE-T'
    | 'CAdES-BASELINE-B'
    | 'CAdES-101733-C'
    | 'CAdES-101733-X'
    | 'CAdES-101733-A'
    | 'PDF-NOT-ETSI'
    | 'PAdES-BASELINE-LTA'
    | 'PAdES-BASELINE-LT'
    | 'PAdES-BASELINE-T'
    | 'PAdES-BASELINE-B'
    | 'PKCS7-B'
    | 'PKCS7-T'
    | 'PKCS7-LT'
    | 'PKCS7-LTA'
    | 'JSON-NOT-ETSI'
    | 'JAdES-BASELINE-B'
    | 'JAdES-BASELINE-T'
    | 'JAdES-BASELINE-LT'
    | 'JAdES-BASELINE-LTA'
    | 'UNKNOWN';
  structuralValidation?: XmlStructuralValidation;
  digestMatchers?: Array<XmlDigestMatcher>;
  basicSignature: XmlBasicSignature;
  signingCertificate?: XmlSigningCertificate;
  certificateChain?: Array<XmlChainItem>;
  contentType?: string;
  mimeType?: string;
  contentIdentifier?: string;
  contentHints?: string;
  signatureProductionPlace?: XmlSignatureProductionPlace;
  commitmentTypeIndications?: Array<XmlCommitmentTypeIndication>;
  signerRole?: Array<XmlSignerRole>;
  policy?: XmlPolicy;
  signaturePolicyStore?: XmlSignaturePolicyStore;
  signerInformationStore?: Array<XmlSignerInfo>;
  signerDocumentRepresentations?: XmlSignerDocumentRepresentations;
  foundCertificates: XmlFoundCertificates;
  foundRevocations: XmlFoundRevocations;
  foundTimestamps?: Array<XmlFoundTimestamp>;
  signatureScopes?: Array<XmlSignatureScope>;
  signatureDigestReference: XmlSignatureDigestReference;
  dataToBeSignedRepresentation?: XmlDigestAlgoAndValue;
  signatureValue?: Array<string>;
  counterSignature?: boolean;
  parent?: XmlSignature;
  duplicated?: boolean;
  daidentifier?: string;
  pdfrevision?: XmlPDFRevision;
}
