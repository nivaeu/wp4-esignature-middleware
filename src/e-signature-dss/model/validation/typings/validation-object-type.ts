/* tslint:disable */
import { POEProvisioningType } from './poe-provisioning-type';
import { POEType } from './poe-type';
import { SignatureValidationReportType } from './signature-validation-report-type';
import { ValidationObjectRepresentationType } from './validation-object-representation-type';

export interface ValidationObjectType {
  objectType:
    | 'CERTIFICATE'
    | 'CRL'
    | 'OCSP_RESPONSE'
    | 'TIMESTAMP'
    | 'EVIDENCE_RECORD'
    | 'PUBLIC_KEY'
    | 'SIGNED_DATA'
    | 'OTHER';
  validationObjectRepresentation: ValidationObjectRepresentationType;
  poe?: POEType;
  validationReport?: SignatureValidationReportType;
  id?: string;
  poeprovisioning?: POEProvisioningType;
}
