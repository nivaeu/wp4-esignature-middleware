/* tslint:disable */
export interface XmlSignerRole {
  role: string;
  notAfter?: string;
  notBefore?: string;
  category?: 'CERTIFIED' | 'CLAIMED' | 'SIGNED';
}
