/* tslint:disable */
export interface AnyType {
  content?: Array<{
    [key: string]: any;
  }>;
}
