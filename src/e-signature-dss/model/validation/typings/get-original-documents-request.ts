/* tslint:disable */
import { DataToValidateDTO } from './data-to-validate-dto';

export type GetOriginalDocumentsRequest<
  TCode extends 'application/json' = 'application/json'
> = TCode extends 'application/json' ? DataToValidateDTO : any;
