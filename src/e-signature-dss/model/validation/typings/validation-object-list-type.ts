/* tslint:disable */
import { ValidationObjectType } from './validation-object-type';

export interface ValidationObjectListType {
  validationObject: Array<ValidationObjectType>;
}
