/* tslint:disable */
import { XmlManifestFile } from './xml-manifest-file';

export interface XmlContainerInfo {
  containerType?: 'ASiC-S' | 'ASiC-E';
  zipComment?: string;
  mimeTypeFilePresent?: boolean;
  mimeTypeContent?: string;
  manifestFiles?: Array<XmlManifestFile>;
  contentFiles?: Array<string>;
}
