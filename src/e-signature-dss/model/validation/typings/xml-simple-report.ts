/* tslint:disable */
import { XmlSemantic } from './xml-semantic';
import { XmlToken } from './xml-token';
import { XmlValidationPolicy } from './xml-validation-policy';

export interface XmlSimpleReport {
  validationPolicy: XmlValidationPolicy;
  documentName?: string;
  validSignaturesCount?: number;
  signaturesCount?: number;
  containerType?: 'ASiC-S' | 'ASiC-E';
  signatureOrTimestamp?: Array<XmlToken>;
  semantic?: Array<XmlSemantic>;
  validationTime?: string;
}
