/* tslint:disable */
import { XmlConclusion } from './xml-conclusion';
import { XmlConstraint } from './xml-constraint';

export interface XmlRFC {
  constraint?: Array<XmlConstraint>;
  conclusion: XmlConclusion;
  title?: string;
  id?: string;
}
