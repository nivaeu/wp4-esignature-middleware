/* tslint:disable */
export interface XmlBasicSignature {
  encryptionAlgoUsedToSignThisToken?:
    | 'RSA'
    | 'DSA'
    | 'ECDSA'
    | 'PLAIN_ECDSA'
    | 'X25519'
    | 'X448'
    | 'EDDSA'
    | 'HMAC';
  keyLengthUsedToSignThisToken?: string;
  digestAlgoUsedToSignThisToken?:
    | 'SHA1'
    | 'SHA224'
    | 'SHA256'
    | 'SHA384'
    | 'SHA512'
    | 'SHA3_224'
    | 'SHA3_256'
    | 'SHA3_384'
    | 'SHA3_512'
    | 'SHAKE128'
    | 'SHAKE256'
    | 'SHAKE256_512'
    | 'RIPEMD160'
    | 'MD2'
    | 'MD5'
    | 'WHIRLPOOL';
  maskGenerationFunctionUsedToSignThisToken?: 'MGF1';
  signatureIntact?: boolean;
  signatureValid?: boolean;
}
