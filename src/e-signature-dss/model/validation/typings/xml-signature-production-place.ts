/* tslint:disable */
export interface XmlSignatureProductionPlace {
  postalAddress?: Array<string>;
  city?: string;
  stateOrProvince?: string;
  postOfficeBoxNumber?: string;
  postalCode?: string;
  countryName?: string;
  streetAddress?: string;
}
