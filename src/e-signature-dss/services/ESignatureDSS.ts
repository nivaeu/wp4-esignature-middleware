import { DataToValidateDTO } from "../model/validation/typings";
import { validateSignedDocument } from "./operations/validateSignedDocument";

export class ESignatureDSS {
  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  baseUrl: string;

  //Operations
  async validateDocument(signedDocument: DataToValidateDTO) {
    try {
      return await  validateSignedDocument(signedDocument, { baseURl: this.baseUrl });
    } catch (err) {
      throw err;
    }
  }
}
