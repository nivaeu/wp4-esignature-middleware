import config from "../../configuration/config";
import { ESignatureDSS } from "./ESignatureDSS";

/**
 * Module to initializa e-signature-dss client with base url from configuration
 */
const {baseUrl, host} = config["e-signature"].eSignatureService
export const ESignatureDSSClient = new ESignatureDSS(
  host + baseUrl
);
