/**
 * Validates signed document against e-signature dss service
 *
 */

import axios from "axios";
import { Logger } from "../../../log";
import {
  DataToValidateDTO,
  XmlConclusion,
  WSReportsDTO,
} from "../../model/validation/typings";

export async function validateSignedDocument(
  signedDocument: DataToValidateDTO,
  config: { baseURl: string }
): Promise<XmlConclusion | Error> {
  const logger = Logger();
  logger.trace("Recieved new signed document to be validated");

  const { baseURl } = config;
  try{
  const validationRequest = await axios.post(
    baseURl + "validation/validateSignature",
    signedDocument,{headers:{
      'Content-Type': 'application/json'
    }}
  );
  console.log(validationRequest)
  logger.trace("Validation executed");

    if (validationRequest) {
      const validationResult: WSReportsDTO = validationRequest.data;
      console.log(validationResult)
      if (validationResult?.DetailedReport?.BasicBuildingBlocks) {
        //once the validation data has been recieved, check if signature is valid
        const result =
          validationResult?.DetailedReport?.BasicBuildingBlocks[0]?.Conclusion;
        logger.trace(
          "Signed document validated successfully, returning validation result"
        );
        logger.trace(
          validationResult?.DetailedReport?.BasicBuildingBlocks[0]?.Conclusion
        );
        return result;
      } else {
        logger.error("Recieved validation result with unknown structure: ");
        logger.error(validationResult);

        return new Error("Recieved validation result with unknown structure");
      }
    } else {
     return new Error("Validation error");
    }
  } catch (err) {
    console.log(err)
    return new Error("Validation error");
  }
}
