import express from "express";
import morgan from "morgan";
import { createProxyMiddleware } from "http-proxy-middleware";
import { signatureValidationInterceptor } from "./interceptor";
import { json } from "body-parser";
import { Configuration, readConfig } from "./configuration";

const main = () => {
  //Create express server
  const app = express();

  // create application/json parser
  var jsonParser = json();

  //Logging
  app.use(morgan("dev"));

  //read configuration
  readConfig();
  //exit if configuration has not been initialized successfully
  if (!Configuration) {
    return;
  }
  //Info GET endpoint
  app.get("/info", (req, res, next) => {
    res.send(
      "This is a proxy service which proxies to Billing and Account APIs."
    );
  });

  //add validation interceptor (or middleware)
  app.use(jsonParser, signatureValidationInterceptor);

  // Proxy endpoints
  Configuration.proxy.proxyRoutes.forEach(({ path, target }) => {
    app.use(
      "",
      createProxyMiddleware({
        target: target,
        changeOrigin: true,
        pathRewrite: {
          [`^/${path}`]: "",
        },
      })
    );
  });

  // Start the Proxy
  app.listen(Configuration.proxy.port, Configuration.proxy.host, () => {
    console.log(
      `Starting Proxy at ${Configuration!.proxy.host}:${
        Configuration!.proxy.port
      }`
    );
  });
};

main();
