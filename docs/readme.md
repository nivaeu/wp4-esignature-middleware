# eSignature Middleware

The eSiganture middleware is a service that stands between two services (server to server or client to server) in order to ensure a secure and certified comunication between them executing validation processes of the information exchanged with the help of the CEF DSS eSignature services. It works as a sort of proxy or middleware, intercepting the requests made to a preconfigures host, and validates the information exchanged in HTTP format expecting it to include a valid signed document, following the standars of this eSignature service.

## What is eSignature

The eSignature building block of the Connecting Europe Facility (CEF) it’s a set of service that helps public administrations and business to accelerate the creation and verification of electronic signatures. More info about it can be found in [CEF eSignature Oficial Documentation](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/What+is+eSignature). Below a diagram showing the main eSignature process can be found.

![eSignature process diagram](./resources/image2018-11-15_9-16-4.png)

## eSignature Middleware Fundamentals

As mentioned before, the main purpose of the middleware is to stands between the comunications of a system or software architecture and to serve as a proxy that validates the authenticity of exchanges information. To do this, this middleware **must** communicate with an instance of CEF DSS (Digital Signature Service) eSignature Web Service in order to make use of its HTTP REST API to execute documents signature validation 

![eSignature process diagram](./resources/eSignature_Middleware_fun.jpeg)

The basic work flow of the middleware when it recieves an incoming request is:

1. Intercepts or captures requests (made with a
standardized format).
2. Validates that information against eSignature DSS services
3. If the validation is correct, then it “allows” the request and validates (or not) the response

The logic followed in each step can be modified setting different HTTP headers and configurations recognized by the middleware in order to apply different validation strategies, that will be commented later in this documentation.

## Middleware instalation and setup.

This middleware software if written in NodeJS. At the moment of writting this documentation, the stable version of NodeJS is v14.17.6.

To setup the middleware NodeJS in version greater than v12.0. Other libraries needed are NPM (usally installed with NodeJS) and [Yarn](https://yarnpkg.com/getting-started/install).

Once yarn has been successfully installed and the repository has been clone, project dependencies must be installed executing command `yarn install` in project root folder (the same directory that contains package.json file).

To execute the middleware after dependencies installations, just execute command `yarn start`. An instance of the middleware will be running in default port 5000. 

## Middlware configuration

Configuration parameters of the middlware will be configured through an yaml file located at the root folder of the project. The content of this file will be validated before every start, causing the service no to initialize if any configuration parameter is not correctly defined.

The following table includes the main configuration parameters:

| Parameter      | Type                                                         | Description                                                                               | Default   | Required |
|----------------|--------------------------------------------------------------|-------------------------------------------------------------------------------------------|-----------|----------|
| proxy.port     | number                                                       | Port th application will be listening to through its HTTP server                          | 5000      | No       |
| proxy.host     | string                                                       | Hostname of the                                                                           | localhost | No       |
| proxy.logLevel | "info" \| "trace" \| "debug" \| "warn" \| "error" \| "fatal" | Log level for the aplication (not applied to configuration validation process at startup) | info      | No       |

